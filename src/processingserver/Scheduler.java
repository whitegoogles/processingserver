package processingserver;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Schedules survey jobs for the entire processing server. This class is 
 * a singleton since you should never have more than one Scheduler.
 * @author daviesj
 */
public class Scheduler {
    private static Scheduler singleton;
    
    private List<Job> pendingJobs;
    private Queue<Job> readyJobs;
    private BlockingQueue<Runnable> runningJobs;
    private Queue<Job> timedOutJobs;
    private ThreadPoolExecutor jobExecutor;
    
    private static final int JOB_CHECK_TIME = 10000;
    private static final int MAX_RUNNING_JOBS = 10;
    private static final int MAX_RUNNING_TIME = 60000;
    
    /**
     * Creates the scheduler, which makes a thread pool and a couple
     * different queues for handling the survey processing jobs.
     */
    private Scheduler(){
        
        //Eventually, read all the jobs from the database so we don't lose them
        //all if the server goes down
        pendingJobs = new ArrayList();
        readyJobs = new PriorityQueue();
        runningJobs = new ArrayBlockingQueue(MAX_RUNNING_JOBS);
        timedOutJobs = new PriorityQueue();
       
        //Create a thread pool for executing the survey jobs in parallel
        jobExecutor = new ThreadPoolExecutor(
                MAX_RUNNING_JOBS,MAX_RUNNING_JOBS,
                MAX_RUNNING_TIME,TimeUnit.SECONDS,runningJobs);
 
        
        //Main scheduling thread that schedules all jobs to run/times them out,
        //etc
        Thread t = new Thread(){
            public void run(){
                while(true){
                    //Move jobs from pending to ready if it is past their execution time
                    //TODO Refactor this to be cleaner (I think you can use an iterator
                    //or something like that)
                    ArrayList jobsToRemove = new ArrayList();
                    for(Job job : pendingJobs){
                        if(job.surveyCompleted()){
                            jobsToRemove.add(job);

                            //Add the job to the ready queue - > Eventually this can be prioritized
                            readyJobs.add(job);
                        }
                    }
                    pendingJobs.removeAll(jobsToRemove);

                    //Move jobs from ready to running (if there is room)
                    if((!readyJobs.isEmpty()) && runningJobs.size()<MAX_RUNNING_JOBS){
                        Job jobToStart = readyJobs.poll();
                        jobExecutor.execute(jobToStart);
                    }


                    try{
                        Thread.sleep(JOB_CHECK_TIME);
                    }
                    catch(Exception e){

                    }

                    //TODO Put timed out jobs in the timed out queue

                    //Eventually, serialize all jobs out to the database as well so
                    //when the server goes done we don't lose them all.
                }
            }
        };
        t.start();
    }
    
    /**
     * Classic singleton pattern for returning the scheduler object.
     * @return the scheduler
     */
    public static Scheduler getScheduler(){
        if(singleton == null){
            singleton = new Scheduler();
        }
        return singleton;
    }
    
    /**
     * Schedules a survey to be processed at the specified date.
     * @param date The date when the survey closes, so it can be processed
     * @param surveyId The id of the survey
     */
    public void schedule(Date date, String surveyId){
        Job job = new Job(date,surveyId);
        pendingJobs.add(job);
    }
}
