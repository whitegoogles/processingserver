package processingserver;

import dal.SurveyAnswer;
import domain.DefaultAlgorithm;
import java.util.Date;

/**
 * A job is an object that holds the survey to be processed and the
 * date it needs to be processed at, and then actually processes those
 * results when triggered by the scheduler.
 * @author daviesj
 */
public class Job implements Runnable,Comparable{
    private Date runDate;
    private String surveyId;
    
    /**
     * Creates a new job that runs after the specified date
     * on the specified surveyId
     * @param date The date after which to run the job
     * @param surveyId The survey to find the results for in the database
     */
    public Job(Date date, String surveyId){
        runDate =date;
        this.surveyId = surveyId;
    }
    
    /**
     * Determines if this job should be run yet or not
     * @return If we are currently past the date when the survey closes
     */
    public boolean surveyCompleted(){
        Date now = new Date();
        return runDate.before(now);
    }

    /**
     * This runs a processing algorithm on the survey answers and then prints
     * out the results.
     */
    @Override
    public void run() {
        
        //TODO Eventually, pull all survey results from the data access layer using surveyId
        SurveyAnswer [] surveyAnswers;
        
        //Instead, I'm just going to make some canned survey result objects
        surveyAnswers = new SurveyAnswer[] {
            new SurveyAnswer(new double [] {
                1,0,1,1,1,1,1,1,0,0,0,0,0,1,0,0,1,0
            }
            ,"A question text"
            ,SurveyAnswer.SurveyQuestionType.CheckboxInput),
            new SurveyAnswer(new double [] {
                1,0,1,1,1,1,1,1,0,0,0,0,0,1,0,0,1,0
            }
            ,"A new question text"
            ,SurveyAnswer.SurveyQuestionType.NumberInput),
            new SurveyAnswer(new double [] {
                1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17
            }
            ,"A asdfn text"
            ,SurveyAnswer.SurveyQuestionType.NumberInput),
            new SurveyAnswer(new double [] {
               14.56,89.999,70,865,123,4,555555
            }
            ,"A ramal"
            ,SurveyAnswer.SurveyQuestionType.OptionsInput),
            new SurveyAnswer(new double [] {
                1,2,3,4,4,3,2,1,1,3,2,2,2,2,2,4,3,3,3,3,1,1,1,1
            }
            ,"suzeey"
            ,SurveyAnswer.SurveyQuestionType.CheckboxInput)
        };
        
        //TODO Eventually pull custom algorithms from the survey object 
        //and parse them with AlgorithmParser, but instead we're just going
        //to use the default one
       String [] surveyResults = new String[surveyAnswers.length];
       for(int i = 0; i<surveyAnswers.length; i+=1){
           surveyResults[i] = (new DefaultAlgorithm()).computeResult(surveyAnswers[i],surveyAnswers);
       }
       System.out.println("Calculated survey results for "+surveyId);
       
       //TODO Eventually we would store this inside the DAL, but instead I'm
       //just going to print it out to the screen
       for(int i = 0; i<surveyResults.length; i+=1){
           //System.out.println("Question: "+surveyAnswers[i].getQuestion());
           //System.out.println("Result: "+surveyResults[i]);
       }
    }

    @Override
    public int compareTo(Object o) {
        return ((Job)o).runDate.compareTo(runDate);
    }
    
}
