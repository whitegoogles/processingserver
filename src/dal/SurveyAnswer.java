package dal;

/**
 * This is in the DAL. It would normally be created by a DAL helper object
 * that would use a survey id to read ALL survey answers into an array of these
 * objects. It is used to hold the answers to a specific question in a survey.
 * @author daviesj
 */
public class SurveyAnswer {
    private double [] answers;
    private String questionText;
    private SurveyQuestionType questionType;
    
    public enum SurveyQuestionType{
        NumberInput,
        OptionsInput,
        CheckboxInput
    };
    
    /**
     * Creates a survey answer object to hold the answers, question text,
     * and type of the current survey question.
     * @param answers The answers from all users to the survey question
     * @param questionText The text of the question
     * @param type The type of the question
     */
    public SurveyAnswer(double [] answers,String questionText,SurveyQuestionType type){
        this.answers = answers;
        this.questionText = questionText;
        this.questionType = type;
    }
    
     public SurveyQuestionType getType(){
        return questionType;
    }
    
    public double[] getAnswers(){
        return answers;
    }
    
    public String getQuestion(){
        return questionText;
    }
    
}
