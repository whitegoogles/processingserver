package domain;

import dal.SurveyAnswer;

/**
 * This is a default algorithm that runs on survey answers for a specific
 * question if no custom algorithm is specified.
 * @author daviesj
 */
public class DefaultAlgorithm implements Algorithm{

    /**
     * It computes the mean, median, integral, and dot product of the
     * answers to the passed in question.
     * @param answers The answers to the specific question
     * @param allAnswers The answers to every question in the survey
     * @return A string holding the JSON output of this algorithm
     */
    @Override
    public String computeResult(SurveyAnswer answers,SurveyAnswer [] allAnswers) {
        Statistics stats = new Statistics();
        LinearAlgebra linAlg = new LinearAlgebra();
        Calculus calc = new Calculus();
        double mean = stats.mean(answers.getAnswers());
        double median = stats.median(answers.getAnswers());
        double integral = calc.integral(answers.getAnswers(), 2);
        double dotProd = linAlg.dotProduct(answers.getAnswers(), answers.getAnswers());
        return "{mean: "+mean+",median:"+median+",integral: "+integral+",dotProduct: "+dotProd+"}";
    }
    
}
