package domain;

/**
 * Helper class for computing discrete integrals/derivatives
 * @author daviesj
 */
public class Calculus {
    
    /**
     * Returns the discrete derivative of a set of values (not sure how to 
     * compute this honestly because it doesn't really exist.....)
     * @param values The values
     * @return Their discrete derivative
     */
    public double derivative(double [] values){
        
        //TODO Write this (its a discrete one so uggg)
        return Double.NaN;
    }
    
    /**
     * Returns the integral of a set of values using Simpson's method
     * @param values The y values 
     * @param interval The length of each x for the y values
     * @return The area underneath the trapezoid(s) defined by values and interval
     */
    public double integral(double [] values, double interval){  
        if(values.length>0){        
            double firstY = values[0];
            double area = 0;
            for(int i = 1; i<values.length; i+=1){      
                double secondY= values[i];
                area += ((firstY+secondY)/2) * interval;
                firstY = secondY;
            }
            return area;
        }
        return Double.NaN;
    }
}

