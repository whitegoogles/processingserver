package domain;

/**
 * Helper class for calculating different values for vectors/matrices
 * @author daviesj
 */
public class LinearAlgebra {
    
    /**
     * (v1)*transpose(v2)
     * If |v1| != |v2|, it returns NaN
     * @param vec1
     * @param vec2
     * @return the dot product of two equal length vectors
     */
    public double dotProduct(double [] vec1, double [] vec2){
        if(vec1.length==vec2.length){
            double curProduct = 0;
            for(int i = 0; i<vec1.length; i+=1){
                curProduct+=vec1[i]*vec2[i];
            }
            return curProduct;
        }
        return Double.NaN;
    }
    
}
