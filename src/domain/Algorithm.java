package domain;

import dal.SurveyAnswer;

/**
 * Defines the algorithm interface which computes the results of a specific
 * survey question.
 * @author daviesj
 */
public interface Algorithm {
    public String computeResult(SurveyAnswer answers,SurveyAnswer [] allAnswers);
}
