package domain;

import java.util.Arrays;

/**
 * Helper class for calculating things like mean/mode/median.
 * @author daviesj
 */
public class Statistics {
    
    /**
     * Computes the average of a bunch of values.
     * @param values The values to compute the average of
     * @return The average
     */
    public double mean(double [] values){
        double sum = 0;
        for(double curVal : values){
            sum+=curVal;
        }
        return sum/values.length;
    }
    
    /**
     * Computes the most occurring number in a set of values
     * @param values The values to find the mode of
     * @return The mode
     */
    public double mode(double [] values){
        
        //TODO Write this
        return Double.NaN;
    }
    
    /**
     * Computes the number that occurs in the middle of a set of values(where
     * middle is either the average of the two middles if even or the actual middle
     * value if odd)
     * @param values The set of values
     * @return The median
     */
    public double median(double [] values){
        Arrays.sort(values);
        if(values.length>0){
            if(values.length%2 ==0){
                return (values[values.length/2]+values[values.length/2-1])/2;
            }
            return values[values.length/2];
        }
        return Double.NaN;
    }
}
